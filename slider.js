(function ($, window) {
    var mainSlider = {
        defaultConfig: {
            batchCount: 0, //это трогать не надо. все равно затрется
            mainWidth: 582, //ширина основного фото в десктопе
            thumbWidth: 294, //ширина миниатюр в десктопе
            currentIndex: 0, //текущая фотка в коллекции (той, что передали в options)
            items: [], //коллекция фоток
            mobileStart: 750, //с этой ширины экрана активируетс мобильная версия
        },

        init: function (container, options) {
            this.config = $.extend({}, this.defaultConfig, options);

            this.$container = $(container); //основной контейнер. Сюда мы все отрисуем
            this.isMobile = false;
            this.timeoutId = 0;

            this._events();
            this.checkMobile();
            this.render();
        },

        //события
        _events: function () {
            $(window).on('resize', this.onResize.bind(this));
            this.$container.on('click', '.main-slider__arrow-left', this.onLeftClick.bind(this));
            this.$container.on('click', '.main-slider__arrow-right', this.onRightClick.bind(this));
        },

        onResize() {
            var self = this;
            clearTimeout(this.timeoutId);
            this.timeoutId = setTimeout(function () {
                self.checkMobile();
                self.render();
            }, 200);
        },

        checkMobile() {
            this.isMobile = window.matchMedia("(max-width: " + this.config.mobileStart + "px)").matches;
            return this.isMobile;
        },

        onLeftClick(e) {
            e.preventDefault();
            if (this.config.currentIndex === 0) {
                this.config.currentIndex = this.config.items.length - 1;
            } else {
                this.config.currentIndex--;
            }
            this.render();
        },

        onRightClick(e) {
            e.preventDefault();
            if (this.config.currentIndex === this.config.items.length - 1) {
                this.config.currentIndex = 0;
            } else {
                this.config.currentIndex++;
            }
            this.render();
        },

        //проверяет сколько колонок миниатюр влезет побокам
        getBatchCount() {
            var batches = Math.ceil(($(window).innerWidth() - this.config.mainWidth) / this.config.thumbWidth);
            if (batches % 2 !== 0) {
                batches++; //нужно на 1 больше, так как будут пустоты по бокам
            }

            this.config.batchCount = batches;
            return this.config.batchCount;
        },

        //создает набор данных для слайдера
        getThumbs() {
            var totalImages = (this.config.batchCount * 2) + 1,
                imagesInSight = this.config.batchCount,
                newItems = this.config.items,
                result = {
                    left: [],
                    right: [],
                    main: {}
                };

            if (this.config.items.length < totalImages) {
                while (newItems.length < totalImages) {
                    newItems = newItems.concat(this.config.items);
                }
            }

            var rightStart = this.config.currentIndex + 1,
                right = newItems.slice(rightStart, rightStart + imagesInSight);

            if (right.length < imagesInSight) {
                right = right.concat(newItems.slice(0, imagesInSight - right.length));
            }
            result.right = right;

            var left = [];
            if (this.config.currentIndex - 1 < imagesInSight) {
                if (this.config.currentIndex < 1) {
                    left = newItems.slice(imagesInSight * -1);
                } else if (this.config.currentIndex < 2) {
                    left = newItems.slice(0, 1);
                    left = newItems.slice((imagesInSight - 1) * -1).concat(left);
                } else {
                    left = newItems.slice(0, this.config.currentIndex);
                    if (left.length !== imagesInSight) {
                        left = newItems.slice((imagesInSight - this.config.currentIndex) * -1).concat(left);
                    }
                }
            } else {
                var leftStep = this.config.currentIndex - 1 - imagesInSight;
                if (this.config.batchCount < 4) {
                    left = newItems.slice(leftStep + 1, imagesInSight + 1);
                } else {
                    left = newItems.slice(leftStep, imagesInSight);
                }
            }
            result.left = left;

            result.main = this.config.items[this.config.currentIndex];
            return result;
        },

        //рендер полученных ранее данных. режим мобилы и десктопа различатся
        render: function () {
            var $sliderWrap = $('<div class="main-slider"></div>'),
                $arrowLeft = $('<a href="#" class="main-slider__arrow-left"></a>'),
                $arrowRight = $('<a href="#" class="main-slider__arrow-right"></a>'),
                $mainImage = $('<div class="main-slider__main-image"> </div>'),
                $leftThumbs = $('<div class="main-slider__left-thumbs"> </div>'),
                $rigthThumbs = $('<div class="main-slider__rigth-thumbs"> </div>');

            $mainImage
                .append($arrowLeft)
                .append($arrowRight);

            this.getBatchCount();
            var data = this.getThumbs();

            if (!this.isMobile) {
                $mainImage.css({backgroundImage: 'url(' + data.main.origin + ')'});

                if (data.left.length && data.right.length) {
                    var counter = 1,
                        leftBatchCount = data.left.length / 2,
                        batch = [],
                        $sliderBatchLeft = null,
                        $sliderBatchRight = null;

                    while (data.left.length > 0) {
                        batch = data.left.splice(0, 2);

                        if(leftBatchCount % 2 !== 0) {
                            if(counter % 2 === 0) {
                                batch = batch.reverse();
                            }
                        } else if(counter % 2 !== 0) {
                            batch = batch.reverse();
                        }

                        $sliderBatchLeft = $('<div class="main-slider__batch"></div>');
                        $.each(batch, function (key, item) {
                            var $thumb = $('<div class="main-slider__thumb"></div>');
                            $thumb.css({backgroundImage: 'url(' + item.thumb + ')'});
                            $sliderBatchLeft.append($thumb);
                        });
                        $leftThumbs.append($sliderBatchLeft);
                        counter++;
                    }

                    counter = 1;
                    while (data.right.length > 0) {
                        batch = data.right.splice(0, 2);
                        if (counter % 2 === 0) {
                            batch = batch.reverse();
                        }

                        $sliderBatchRight = $('<div class="main-slider__batch"></div>');
                        $.each(batch, function (key, item) {
                            var $thumb = $('<div class="main-slider__thumb"></div>');
                            $thumb.css({backgroundImage: 'url(' + item.thumb + ')'});
                            $sliderBatchRight.append($thumb);
                        });
                        $rigthThumbs.append($sliderBatchRight);
                        counter++;
                    }
                }

                $sliderWrap
                    .append($leftThumbs)
                    .append($mainImage)
                    .append($rigthThumbs);

                $sliderWrap.removeClass('main-slider_mobile');
                if (this.config.batchCount < 4) {
                    $sliderWrap.addClass('main-slider_2-batch');
                } else {
                    $sliderWrap.removeClass('main-slider_2-batch');
                }
            } else {
                $mainImage.append('<img src="' + data.main.origin + '"/>');

                $sliderWrap.append($mainImage);
                $sliderWrap.addClass('main-slider_mobile').removeClass('main-slider_2-batch');
            }

            this.$container.html($sliderWrap);
        }
    };

    $.fn.mainSlider = function (options) {
        return Object.create(mainSlider).init(this, options);
    }
})($, window);